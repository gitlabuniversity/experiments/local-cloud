# Install Acorn with K3d

- 🌍 [https://k3d.io](https://k3d.io)
- 🌍 [https://acorn.io](https://acorn.io)

## Requirements

### Install tha Acorn CLI

```bash title="macOS & linux"
curl https://get.acorn.io | sh
```

### Install K3d
!!! info
    you need to install Docker before

```bash title="Install"
curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
```

```bash title="Create the cluster"
k3d cluster create panda --api-port 6550 -p "80:80@loadbalancer"
```
!!! info
    `--api-port 6550 -p "80:80@loadbalancer"` allows to configure the cluster to proxy traffic from localhost to the cluster

```bash title="output"
INFO[0000] Prep: Network
INFO[0000] Created network 'k3d-panda'
INFO[0000] Created image volume k3d-panda-images
INFO[0000] Starting new tools node...
INFO[0001] Creating node 'k3d-panda-server-0'
INFO[0001] Pulling image 'ghcr.io/k3d-io/k3d-tools:5.4.6'
INFO[0002] Pulling image 'docker.io/rancher/k3s:v1.24.4-k3s1'
INFO[0004] Starting Node 'k3d-panda-tools'
INFO[0011] Creating LoadBalancer 'k3d-panda-serverlb'
INFO[0011] Pulling image 'ghcr.io/k3d-io/k3d-proxy:5.4.6'
INFO[0015] Using the k3d-tools node to gather environment information
INFO[0015] Starting new tools node...
INFO[0015] Starting Node 'k3d-panda-tools'
INFO[0017] Starting cluster 'panda'
INFO[0017] Starting servers...
INFO[0017] Starting Node 'k3d-panda-server-0'
INFO[0022] All agents already running.
INFO[0022] Starting helpers...
INFO[0022] Starting Node 'k3d-panda-serverlb'
INFO[0028] Injecting records for hostAliases (incl. host.k3d.internal) and for 3 network members into CoreDNS configmap...
INFO[0031] Cluster 'panda' created successfully!
INFO[0031] You can now use it like this:
kubectl cluster-info
```

#### Check

```bash title="check the setup"
kubectl cluster-info
```

```bash title="output"
Kubernetes control plane is running at https://0.0.0.0:63142
CoreDNS is running at https://0.0.0.0:63142/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://0.0.0.0:63142/api/v1/namespaces/kube-system/services/https:metrics-server:https/proxy
```

```bash title="check the setup"
kubectl get nodes
```

```bash title="output"
k3d-panda-server-0   Ready    control-plane,master   88s   v1.24.4+k3s1
```

## Install Acorn

```bash title="Install Acorn"
acorn install --cluster-domain '.local.on-acorn.io:80'
```

```bash title="output"
  ✔  Running Pre-install Checks
  ✔  Installing ClusterRoles
  ✔  Installing APIServer and Controller (image ghcr.io/acorn-io/acorn:v0.3.1)
  ✔  Waiting for controller deployment to be available
  ✔  Waiting for API server deployment to be available
  ✔  Running Post-install Checks
  ✔  Running Post-install Checks
  ✔  Installation done
```
